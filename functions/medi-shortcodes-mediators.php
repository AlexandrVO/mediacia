<?php 
add_shortcode( 'mediators', 'mediators_function');

function mediators_function()
{
	$str='	<div class="container-fluid post-single ">
				<div class="row">
					<div class="col-sm-12">';
	$args = array(
			    'post_type' => 'participant',
			    'role'=>'mediator',
			    );
	$query = new WP_Query();
	$my_post = $query->query($args);
	$str.='<div class="fixed-area-main">
		 	    <div class="owl-carousel owl-theme" id="sync1">';
	foreach ($my_post as $item){
		$thumbId = get_post_thumbnail_id($item->ID);
		$thymbUrl = wp_get_attachment_url($thumbId, 'full', true);
		$category = get_terms($item);
		$str.= '<div class="item">
                    <img src="'.$thymbUrl.'" alt="'.$item->post_title.'" class="item-image">
                    <div class="item-pink">
                    </div>
                    <div class="item-body">
                        <div class="item-title">										
                            <p>'.$item->post_title.'</p>
                            <p class="item-content">'.$item->post_content.'</p>										 
                        </div>									 
                    </div>
                </div>';
	}
	$str.=' 			</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="fixed-area">
                        <div class="owl-carousel" id="sync2">';
	foreach ($my_post as $item){
	    $thumbId = get_post_thumbnail_id($item->ID);
	    $thymbUrl = wp_get_attachment_url($thumbId, 'full', true);
		$category = get_terms($item);
			$str.= '<div class="item">
                        <div class="item-image">
                            <img src="'.$thymbUrl.'" alt="'.$item->post_title.'" class="item-image">
                         </div>
                        <div class="item-body">
                            <div class="item-title">'.$item->post_title.'</div>
                        </div>
                    </div>';
	}
	return $str.=' </div>
                </div>
            </div>
        </div>
    </div>';
}