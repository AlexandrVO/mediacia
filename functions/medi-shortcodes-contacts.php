<?php
add_shortcode('contacts', 'contacts_function');

function contacts_function()
{
	echo '<div class="contacts-info-block">
		    <p class="contacts-small-text">Адрес</p>
			    <div class="adress-image"><img src="'.get_template_directory_uri().'/img/a.svg" ></div>
				    <div class="contacts-adress-text">'.get_theme_mod('adress').'
					</div>
					<div>
						<p class="contacts-small-text">Телефоны</p>
						<div class="tel-image"><img src="'.get_template_directory_uri().'/img/tel.svg" ></div>
						<span class="contacts-adress-text-tel">'.get_theme_mod('header_phone').'</span>
						<span class="contacts-adress-text-tel">'.get_theme_mod('phone_contact').'</span>
					</div>
					<div>
						<p class="contacts-small-text">Электронная почта</p>
						 <div class="adress-image"><img src="'.get_template_directory_uri().'/img/mail.svg" ></div>
						<p class="contacts-adress-text-mail">'.get_theme_mod('header_mail').'</p>
					</div>
				</div>
					<hr>
					<div class="contacts-info-block2">
					<p class="contacts-data-title-text">Персональные контакты</p>
				</div>';
    $args = array(
                'post_type' => 'participant',
                'role'=>'contacts',
                );
    $query = new WP_Query();
    $my_post = $query->query($args);
    foreach ($my_post as $item){
        $telephone=get_post_meta($item->ID, 'telephone');
        $mail=get_post_meta($item->ID, 'email');
        echo '<div>
                <div class="contacts-role-name default-role" id="'.$item->ID.'">
                    <div class="img-cont" id="img-cont">
                        <img src="'.get_template_directory_uri().'/img/cont.svg" id="cont'.$item->ID.'" class="cont" >
                        <img  class="cont-act" src="'.get_template_directory_uri().'/img/cont-act.svg" id="cont-act'.$item->ID.'" >
                    </div>
                <span class="cont-text">'.$item->post_title.'</span></div>
                <div class="contacts-role-info" id="info'.$item->ID.'">
                    <p contacts-role-tel>'.$telephone[0].'</p>
                    <p class="contacts-role-mail">'.$mail[0].'</p>
                </div>
            </div>';
    }
}