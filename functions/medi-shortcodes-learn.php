<?php
add_shortcode( 'learn', 'learn_function' );

function learn_function()
{
	echo '<div class="container-fluid post-single">';
	$args = array(
		    'post_type' => 'post',
            'category_name'=>'learn',
            'orderby' => 'post_title'
		    );
	$query = new WP_Query();
	$my_post = $query->query($args);
	foreach ($my_post as $item) {
		$thumbId = get_post_thumbnail_id($item->ID);
		$thymbUrl = wp_get_attachment_url($thumbId);
		echo '
            <div class="row school-block">
                <div class="col-12">
                <div class="school-title">
                <a href="'.get_permalink($item->ID).'">'.$item->post_title.'</a>
                </div>
                    <div class="school-content">
                        <span>'.apply_filters( 'the_content', $item->post_excerpt).'</span>
                    </div>
                </div>
            </div>';
	}
    $args2 = array(
        'post_type' => 'participant',
        'role'=>'mediator',
        );
	$query2 = new WP_Query();
	$my_post2 = $query->query($args2);
	echo '<div class="row">
            <div class="col-12">
                <p class="the-title2">Наши медиаторы</p>
            </div>
          </div>
			<div class="fixed-area-main">
	 			<div class="owl-carousel owl-theme" id="sync3">';
    foreach ($my_post2 as $item2){
    $thumbId2 = get_post_thumbnail_id($item2->ID);
    $thymbUrl2 = wp_get_attachment_url($thumbId2, 'full', true);
    $category2= get_terms($item2);
    echo '
        <div class="item">
            <img src="'.$thymbUrl2.'" alt="'.$item2->post_title.'" class="item-image">
                <div class="item-pink"></div>
                <div class="item-body">
                <div class="item-title">
                    <p>'.$item2->post_title.'</p>
                    <p class="item-content">'.$item2->post_content.'</p>
                </div>
            </div>
        </div>';
	}
    echo '    </div>
            </div>
         </div>
    </div>';
}



