<?php
add_action('wp_ajax_eventpost','event_function');
add_action('wp_ajax_nopriv_eventpost','event_function');
function event_function(){
    $userID=get_current_user_id();
    if (isset($_POST['event'])) {
        $data = $_POST['event'];
        $typ=$_POST['typ'];
        // echo $data;
        if($typ==="event") {
            add_user_meta($userID, 'event', $data, false);
            $data1 = get_post_meta($data, 'places', true);
            $temp = explode(",", $data1);
            $fullData=($temp[0]-1).','.$temp[1];
            update_post_meta($data, "places", $fullData);
        }
        if($typ==="learn") {
            add_user_meta($userID, 'learns', $data, false);
        }
    }
}
						
add_action('wp_ajax_eventpostdelete','event_delete_function');
add_action('wp_ajax_nopriv_eventpostdelete','event_delete_function');

function event_delete_function(){
    $userID=get_current_user_id();
    if (isset($_POST['event'])) {
        $data = $_POST['event'];
        delete_user_meta($userID,'event', $data);
    }
}

add_action('wp_ajax_finddoc','find_doc_function');
add_action('wp_ajax_nopriv_finddoc','find_doc_function');

function find_doc_function(){
    $userID=get_current_user_id();
    if (isset($_POST['search'])) {
        $data = $_POST['search'];
        global $wpdb;
        if($data==null) {
            $comment = $wpdb->get_results($wpdb->prepare("SELECT $wpdb->posts.ID,
            $wpdb->posts.post_title, $wpdb->postmeta.meta_value,
            $wpdb->postmeta.meta_value FROM $wpdb->posts INNER JOIN
            $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
            WHERE $wpdb->postmeta.meta_key='_document-input'")) ;
        } else {
            $link = '%' . $wpdb->esc_like( $data ) . '%';
            $comment = $wpdb->get_results($wpdb->prepare("SELECT $wpdb->posts.ID,
            $wpdb->posts.post_title, $wpdb->postmeta.meta_value,
            $wpdb->postmeta.meta_value FROM $wpdb->posts INNER JOIN
            $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
            WHERE $wpdb->postmeta.meta_key='_document-input'
            AND $wpdb->posts.post_title LIKE %s", $link )) ;
        }
        echo json_encode($comment);
     }
}

add_action('wp_ajax_sort','sort_function');
add_action('wp_ajax_nopriv_sort','sort_function');

function sort_function() {
    if (isset($_POST['btn'])) {
        $data = $_POST['btn'];
        global $wpdb;
        $comment = $wpdb->get_results($wpdb->prepare("SELECT $wpdb->posts.post_name,
        $wpdb->posts.post_title, $wpdb->postmeta.meta_value,
        $wpdb->postmeta.meta_value FROM $wpdb->posts INNER JOIN
        $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
        WHERE 
        case '$data'
        WHEN 'pop' THEN $wpdb->postmeta.meta_key='places'
        else  $wpdb->postmeta.meta_key='event_date'
        END
        ORDER BY 
        (case '$data'
        WHEN 'alf' THEN $wpdb->posts.post_title
        else $wpdb->postmeta.meta_value
        END)
        "));
    }
    echo json_encode($comment);
}
