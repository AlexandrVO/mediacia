<?php
function get_week($date){
$days = [
    'ВС', 'ПН', 'ВТ', 'СР',
    'ЧТ', 'ПТ', 'СБ'
];

    echo $days[ date("w", strtotime($date) )];
}

function get_month($date){
$days = [
    'Декабря', 'Января','Февраля', 'Марта', 'Апреля',
    'Мая', 'Июня', 'Июля','Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
];

    echo $days[ date("n", strtotime($date) )];
}