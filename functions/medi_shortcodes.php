<?php
add_shortcode( 'program', 'program_function');

function program_function()
{
    $str= '<div class="row">
            <div class="col-sm-12 ">
                <div>
                    <p class="post-head post-single">Содержание программы</p>
                </div>
             </div>
            </div>
            <div class="row">
                <div class="col-sm-12 no-margin no-padding">
                     <div class="learn-box">';
    $args2 = array(
                'post_type' => 'program',
                'learn'=>get_the_ID(),
                'orderby' => 'post_title'
                );
    $query = new WP_Query();
    $my_post = $query->query($args2);
    $k=1;
    $i=0;
     foreach ($my_post as $item) {
         if($i==0)
            $str.= '<div class="learn-item active-learn learn-click" id="'.$item->ID.'">';
         else $str.= '<div class="learn-item learn-click" id="'.$item->ID.'">';
        $str.= '<p class="modul-text">
            Модуль '.$k.'
                </p>
                <p class="modul-title">'.$item->post_title.'</p>
            </div>';
            $k++;
            $i++;
     }
    $str.= '    </div>
            </div>
        </div>
        <div class="row no-padding no-margin back-grey">
            <div class="col-sm-12 col-lg-6 col-md-9 offset-lg-3 offset-md-2 ">
                <div class="modul-content-block">';
        $m=0;
    foreach ($my_post as $item){
        if($m==0)
        $str.= '<div class="modul-content" id="block'.$item->ID.'" style="display:block">';
        else $str.= '<div class="modul-content" id="block'.$item->ID.'" style="display:none">';
        $str.= '    <span>'.$item->post_content.'</span>
                  </div>';
        $m++;
    }
    $str.= '<div class="dop-text">
                <p>По итогам обучения слушателям программы выдается удостоверение ФГБОУ ВО РГЭУ (РИНХ) о повышении квалификации по направлению «Психолого-педагогическое образование».</p>
            </div>
           </div>
          </div>
        </div>';
    return $str;
}

add_shortcode( 'documents', 'documents_function');

function documents_function()
{
	$str='<div class="container-fluid no-padding no-margin">
				<div class="row post-single">
					<div class="col-sm-9" id="update1">';
    $args = array(
    'post_type' => 'document',
    );
    $query = new WP_Query();
    $my_post = $query->query($args);
    foreach ($my_post as $item) {
        $met=get_post_meta($item->ID,'_document-input',true);
        $temp = explode(",", $met);
        $str.= '
        <input type="hidden" value="'.get_template_directory_uri().'/img/doc.svg" id="doc-img">
        <img src="'.get_template_directory_uri().'/img/doc.svg" >
        <a href="'.$temp[0].'" class="post-title">'.$item->post_title.'</a>
        <div class="doc-parametrs-text">
            <span class="">Размер: '.$temp[1].'</span> Формат: <span class="format">'.pathinfo($temp[0], PATHINFO_EXTENSION).'</span>
        </div>';
    }
    echo '</div>';
	$str.='	</div>
				<div class="col-sm-3 doc-input-block">
					<div class="find-text"><p>Поиск</p>
				</div>
				<div>
					<input type="text" placeholder="по названию документа" id="find_doc" class="find-doc" name="find_doc">
				</div>
			</div>';
	return $str;
}

add_shortcode('cabinet','cabinet_function');

function cabinet_function()
{
	$met=get_user_meta(get_current_user_id(), 'event');
	$met2=get_user_meta(get_current_user_id(), 'learns');
	echo '	<div class="container-fluid no-padding post-single">';
	if (empty($met)) { echo '
				<div class="the-title"><b>Мои мероприятия</b></div>
				<div class="event-no">Мероприятия не выбраны</div>
				<div class="see-learn-link-main">
					<a href="../creation-territory/meropriyatiya/"><span class="see-learn-link">Посмотреть ближайшие мероприятия</span></a>
				</div>
				<input type="hidden" id="empty" value="empty">';
	}
	else{
	    echo '<span class="the-title"><b>Мои мероприятия</b><span><input type="hidden" id="empty" value="Notempty">
				<div class="row">';
	    foreach ($met as $m=>$value){
        $my_post = get_post($value);
        $thumbId = get_post_thumbnail_id($my_post->ID);
        $thymbUrl = wp_get_attachment_url($thumbId);
        $temp = explode("-", get_post_meta($my_post->ID, 'event_date', true));
        $met= get_post_meta($my_post->ID, 'event_date', false);
        echo '<div class="col-lg-4 col-md-6 col-sm-12">
                    <a href="'.get_permalink($my_post->ID).'" class="lich-event-link">
                        <div class="lich-event-box">
                            <div class="lich-event-title">'.$my_post->post_title.'</div>
                            <div class="date-lich">
                                <span class="date-lich1">'. intval($temp[2]).'</span><span class="date-lich2">';echo get_month($met[0]);echo'</span>
                            </div>
						</div>
					</a>
			</div>';
	    }
	 echo '	</div>';
	}
	if (empty($met2)) {
	    echo '<div class="row no-margin learn-title-lich">
					<div class="the-title "><b>Обучение</b></div>
				</div>
				<div class="event-no">Обучение не выбраны</div>
				<div class="see-learn-link-main">
					<a href="../creation-territory/obucheniya/"><span class="see-learn-link">Выбрать обучение</span></a>
				</div>
				<input type="hidden" id="empty" value="empty">';
	}
	else {
	    echo '  <span class="the-title">
				<b>Обучение</b>
			    </span>
				<input type="hidden" id="empty" value="Notempty">';
	    foreach ($met2 as $m2=>$value2){
            $my_post2 = get_post($value2);
            $thumbId2= get_post_thumbnail_id($my_post2->ID);
            $thymbUrl2 = wp_get_attachment_url($thumbId2);
            echo '
                <div class="row no-margin lich-learn-box">
                    <a class="lich-learn" href="'.get_permalink($my_post2->ID).'">'.$my_post2->post_title.'</a>									
                </div>';
	    }
	}
}
	
add_shortcode( 'about-center', 'about_function');

function about_function()
{
    echo '<div class="container-fluid no-margin no-padding post-single">
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <div class="about-text-row1">
                    Деятельность Центра строится на основе анализа текущей ситуации в образовательных учреждениях. Основные виды деятельности Центра соответствуют Дорожной карте по развитию служб примирения (медиации) в образовательных организациях Ростовской области в рамках реализации Концепции развития  сети служб медиации, утвержденной Распоряжением Правительства РФ от 30.07.2014 № 1430.
                    </div>
                </div>
                <div class="col-md-5 about-text-row1-small">
                    <div>
                    <b>Медиация</b> — является действенным инструментом формирования безопасной среды в образовательном учреждении, предупреждения конфликтов и развития позитивного общения в школьном социуме. 
                    </div>
                </div>
            </div>
			<div class="row justify-content-center mt-5">
				<div class="col-md-12 col-lg-8 col-sm-12  about-text-row2 justify-content-center">
					<div class="tex-row-2-box">
						<div class="tex-row-2-title">
							Цель центра
						</div>
						<div class="tex-row-2-small">
							обеспечение эффективной сетевой проектной деятельности по развитию медиации в образовании, культуры созидания в молодёжной среде и социальных инициатив  подрастающего поколения.
						</div> 
					</div>
				</div>
				<div class="grey-role">
				</div>
		</div>
		<div class="row">
			<div class="row-3">
			Мероприятия Центра адресованы
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="row-5-box">
				<div class="row-5-text">
					<span class="text-purple">директорам</span>
					школ
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-5-box">
				<div class="row-5-text">
					<span class="text-purple">заместителям</span>
					 по вопросам безопасности, а также учебно-воспитательной и воспитательной работе
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-5-box">
				<div class="row-5-text">
					<span class="text-purple">социальным педагогам</span>
					 и педагогам-организаторам
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-5-box">
				<div class="row-5-text">
					<span class="text-purple">классным руководителям</span>
					, вожатым, методистам, психологам, уполномоченным по правам ребёнка
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 under-event">
				<span>а также всем педагогам и сотрудникам образовательных учреждений, взаимодействующих с детскими и подростковыми коллективами в рамках учебной и внеклассной работы.</span>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="row-6">
				Основные виды деятельности центра
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 row-7-box">
				<div class="row-7-img">
				<img src="'.get_template_directory_uri().'/img/doing1.svg">
				</div>
				<div class="row-7-text">
					Консалтинговая
				</div>	
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/doing2.svg">
				</div>
				<div class="row-7-text">
					Образовательная
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/doing3.svg">
				</div>
				<div class="row-7-text">
					Просветительская
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/doing4.svg">
				</div>
				<div class="row-7-text">
					Экспертно-аналитическая
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/doing5.svg">
				</div>
				<div class="row-7-text">
					Научно-исследовательская
				</div>
			</div>
		</div>
		<div class="row">
			<div class="row-6">
				Функции центра
			</div>
		</div>
		<div class="row">
			<div class="row-8 func-center" id="func1">
				Научно-методическое и информационно-аналитическое сопровождение деятельности государственных, общественных и образовательных учреждений, решающих актуальные задачи развития служб примирения (медиации) в школах, а также осуществляющих профилактику безнадзорности и правонарушений несовершеннолетних
			</div>
		</div>
		<div class="func-center-under"  id="underfunc1">
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
					создание программ дополнительного профессионального образования и повышения квалификации, нацеленных на развитие специфических профессиональных и социально-психологических компетенций сотрудников служб примирения (медиации) в школах, а также специалистов, осуществляющих профилактику безнадзорности и правонарушений несовершеннолетних;

				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
					организация обучающих семинаров, вебинаров, практикумов, форумов (в том числе в режиме он-лайн)  по вопросам медиации;
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
				разработка инновационных просветительских программ и технологий, нацеленных на правовое и психологическое просвещение  подростков и молодёжи в целях развития культуры созидания;
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
					создание, формирование и применение в социально-образовательном процессе обучающих онлайн-ресурсов, электронных учебников, библиотек.
				</div>
			</div>
		</div>
		<div class="row">
			<div class="row-8 func-center" id="func2">
				Консалтинговое сопровождение деятельности государственных, общественных и образовательных учреждений, решающих актуальные задачи в рамках развития служб примирения (медиации) в школах, а также осуществления профилактику безнадзорности и правонарушений несовершеннолетних
			</div>
		</div>
		<div class="func-center-under"  id="underfunc2">
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
					создание совместных межведомственных социальных, информационных и аналитических проектов, консолидирующих усилия органов государственной власти, образовательных учреждений и институтов гражданского общества
				</div>
			</div>
		
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
				разработка программ и методик мониторинга эффективности применения технологий медиации в целях обеспечения безопасности образовательной среды, эффективности профилактики безнадзорности и правонарушений несовершеннолетних
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-8 row-9">
					формирование экспертной базы для проведения аналитических исследований и повышения квалификации работников образования, а также  специалистов, осуществляющих профилактику безнадзорности и правонарушений несовершеннолетних.
				</div>
			</div>
		</div>
		<div class="row">
			<div class="row-8 func-center" id="func3">
				Информационное сопровождение деятельности школьных служб примирения и продвижение ценностей созидания в молодёжной среде
			</div>
		</div>
			<div class="func-center-under"  id="underfunc3">
				<div class="row justify-content-center">
					<div class="col-md-8 row-9">
						разработка концепции тематического раздела на сайте РГЭУ РИНХ и стратегий освещения деятельности Центра медиации: «Территория созидания» в информационном пространстве, включая ресурсы органов государственной власти, он-лайн ресурсы организаций-партнёров проекта
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-8 row-9">
						насыщение уникальным контентом тематического раздела сайта РГЭУ РИНХ: «Территория созидания»,  продвижение в информационном пространстве  инновационной деятельности Центра
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-8 row-9">
					формирование  инициативной группы блогеров, из числа студентов,  преподавателей, школьных медиаторов, популяризирующих данную тематику в студенческих группах в соцсетях, на личных страницах, тематических форумов, а также осуществление консалтингового сопровождения их деятельности
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-8 row-9">
						формирования экспертной группы из числа сотрудников РГЭУ РИНХ , органов государственной власти, образовательных и общественных организаций  для подготовки тематических интервью, комментариев,  видео-сюжетов
					</div>
				</div>
			</div>
	</div>';
}

add_shortcode( 'creation', 'creation_function');

function creation_function(){
echo '
		<div class="row no-margin post-single-left">
			<div class="col-lg-7 col-md-12 no-padding">
				<div class="tex-row-2-box-cr">
					<div class="tex-row-2-title-cr">
						Цель проекта
					</div>
					<div class="tex-row-2-small-cr">
						Развитие культуры восстановительных взаимоотношений в школе, содействие в обеспечении безопасности образовательной среды, создание благоприятных условий для развития медиативного мировоззрения, основанного на понимании и стремлении к консенсусу, формирование социальных компетенций обучающихся, обеспечивающих конструктивный диалог и продуктивное сотрудничество.
					</div>
				</div>
			</div> 
			<div class="col-lg-5 col-md-12 no-padding">
			<div class="cr-grey">
				<div class="about-text-row1-small-cr"> Миссия проекта</div>
				<div class="row no-margin about-text-row1-small-2-box-cr"> 
						<div class="col-md-4 about-text-row1-small-2-cr">безопасное будущее</div> 
						<div class="col-md-4 about-text-row1-small-2-cr">согласие</div>
						<div class="col-md-4 about-text-row1-small-2-cr">солидарность</div>
				</div>
				<div class="row-2-about">
					Проект создан в соответствии со стратегическим ориентирами воспитания, сформулированным Президентом РФ В. В. Путиным. Данный документ закрепляет необходимость воспитывать новые поколения в духе мирного созидания. 
				</div>
			</div>
		</div>
	</div>
		<div class="row no-margin">
			<div class="row-6 post-single">
				Основные задачи проекта
			</div>
		</div>
		<div class="row no-margin post-single">
			<div class="col-lg-6 task-boxes1">
				<div class=" task-boxes">
					Формирование созидательной активности, воспитание ценностей сотрудничества и солидарности у подрастающего поколения
				</div>
			</div>
			<div class="col-lg-6 task-boxes1">
				<div class=" task-boxes">
					Развитие прикладных исследований и экспертиз
				</div>
			</div>
			<div class="col-lg-6 task-boxes1">
				<div class=" task-boxes ">
					Создание междисциплинарных и межведомственных научно-
					исследовательских проектов
				</div>
			</div>
			<div class="col-lg-6 task-boxes1">
				<div class=" task-boxes">
					Медиативное, консалтинговое и социально-психологическое
					сопровождение деятельности служб школьной медиации.
				</div>
			</div>
			<div class="col-lg-6 task-boxes1">
				<div class=" task-boxes">
					Проведение информационно-творческих, социальных, культурно-просветительских 
					мероприятий, направленных на развитие созидательной активности подрастающего поколения
				</div>
			</div>
		</div>
		<div class="row no-margin post-single">
			<div class="row-6">
				Ожидаемый результат реализации проекта
			</div>
		</div>
			<div class="row no-margin post-single">
			<div class="col-lg-4 col-md-6 row-7-box">
				<div class="row-7-img">
				<img src="'.get_template_directory_uri().'/img/cr-1.svg">
				</div>
				<div class="row-7-text1">
					Оценка результативности деятельности служб школьной медиации
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-2.svg">
				</div>
				<div class="row-7-text1">
					Анализ эффективности
					профилактики самовольных уходов несовершеннолетних из семей и государственных учреждений
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-3.svg">
				</div>
				<div class="row-7-text1">
					Совершенствование системы мониторинга межнациональных отношений и своевременного предупреждения конфликтов на межэтнической и межнациональной почве
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-4.svg">
				</div>
				<div class="row-7-text1">
					Создание эффективного механизма наблюдения и профилактики потенциальной напряженности в полиэтнической и поликонфессиональной среде, предупреждения форм деструктивного социального поведения представителей различных этнокультурных групп
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-5.svg">
				</div>
				<div class="row-7-text1">
					Разработка совместных междисциплинарных и межведомственных научно-
					исследовательских, информационно-творческих, социальных, культурно-
					просветительских проектов, на основе медиативных технологий
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-6.svg">
				</div>
				<div class="row-7-text1">
					Формирование экспертной базы для проведение аналитической деятельности и повышения квалификации работников образования
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
			<div class="row-7-img row-7-box">
				<img src="'.get_template_directory_uri().'/img/cr-7.svg">
				</div>
				<div class="row-7-text1">
					развитие информационно-аналитических ресурсов, направленных на популяризацию культуры восстановительных взаимоотношений и технологий медиации.
				</div>
			</div>
		</div>
		</div>';
}

