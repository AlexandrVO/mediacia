<?php

function pagination($pages = '', $range = 4)
{
     $showitems = ($range * 2)+1;
     global $paged;
     if(empty($paged)) $paged = 1;
     if('' == $pages) {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages) {
             $pages = 1;
         }
     }
     if(1 != $pages) {
         echo '<div class="pagination">';
         for ($i=1; $i <= $pages; $i++) {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                 echo ($paged == $i)? '<a class="link-item active-l">'.$i.'</a>':'<a href="'.get_pagenum_link($i).'" class="link-item default">'.$i.'</a>';
             }
         }
         echo '</div>';
     }
}
