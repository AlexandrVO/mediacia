<?php
add_shortcode( 'news', 'news_function' );

function news_function()
{
	echo '	<div class="container-fluid">
				<div class="row  post-single">';
	$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
            'post_type' => 'post',
            'category_name'=>'news',
            'paged'=> $current_page // текущая страница
            );
    $query = new WP_Query();
    $my_post = $query->query($args);
    foreach ($my_post as $item){
    $thumbId = get_post_thumbnail_id($item->ID);
    $thymbUrl = wp_get_attachment_url($thumbId);
    $met= get_post_meta($item->ID, 'news_date', false);
    $temp = explode("-", get_post_meta($item->ID, 'news_date', true));
    echo '<div class="col-lg-4 col-md-6 col-sm-12 no-margin no-padding border-col">
                <a href="'.get_permalink($item->ID).'" class="news-link-block">
                    <div class="news-block">												
                        <img src="'.$thymbUrl.'" alt="'.$item->post_title.'"  class="small-img img-responsive" align="center">
                        <div class="excerpt">'.apply_filters( 'the_content', $item->post_excerpt).'</div>
                        <p class="news-dig">
                        <span>'.intval(get_the_date('d')).'</span>
                        <span>'.get_the_date('F').'</span>
                        <span>'.get_the_date('Y').'г.</span></p>
                    </div>
                 </a>
            </div>';
    }
    echo '	</div>
                <div class="row justify-content-center">';
                    pagination($query->max_num_pages);
    echo'		</div>';
}




