<?php

add_action('add_meta_boxes', function()
 {
    add_meta_box(
        'metabox_document',
        'Добавить документ',
        'metabox_document_showup',
        'document',
        'side',
        'default'
    );

    add_meta_box(
        'metabox_media',
        'Добавить изображения(для подробной новости)',
        'metabox_media_showup',
        'post',
        'side',
        'default'
    );

    add_meta_box(
        'metabox_date',
        'Дата (формат: год-месяц-день)',
        'metabox_date_showup',
        'post',
        'side',
        'default'
    );

    add_meta_box(
        'metabox_place',
        'Количество мест(для мероприятий)',
        'metabox_places_showup',
        'post',
        'side',
        'default');

    add_meta_box(
        'metabox_page',
        'Показать на промо (для мероприятий)',
        'metabox_page_showup',
        'post',
        'side',
        'default');

    add_meta_box(
        'metabox_main',
        'Показать на главной(для всех)',
        'metabox_main_showup',
        'post',
        'side',
        'default'
    );

    add_meta_box(
        'metabox_link',
        'Добавить ссылку (для сторонних ссылок)',
        'metabox_link_showup',
        'post',
        'side',
        'default'
    );
});

add_action( 'delete_post', 'usermeta_delete' );

function usermeta_delete( $postID )
{
	$users=get_users();
	$terms=get_the_terms($postID,'category');
	foreach ($users as $user) {
		delete_user_meta( $user->ID, 'event', $postID );
		delete_user_meta( $user->ID, 'learns', $postID );

	}
}

add_action('save_post', 'metabox_document_save');

function metabox_document_save($postID)
{
	$names = array("document-input");
	foreach ($names as $key => $value) {
		if (isset($_POST[$value])) {
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				return;

			// не ревизию ли сохраняем?
			if (wp_is_post_revision($postID))
				return;

			// проверка достоверности запроса
			check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');

            // коррекция данных
            $data = sanitize_text_field($_POST[$value]);

            update_post_meta($postID, "_$value",$data);
		}
	}
}

function metabox_document_showup($post, $box)
{
	wp_nonce_field('metabox_my_work_action', 'metabox_my_work_nonce');
  	$data = get_post_meta($post->ID, '_document-input',true);
	echo '<div class="metabox-container">';
	echo '<div class="document-container">';
	$temp = explode(",", $data);
	if(!$data) {
        echo '<div class="button-container"><button id="adddocument" class="button button-primary add">Добавить </button></div>';
	} else {
	    $filename= pathinfo($temp[0], PATHINFO_FILENAME); //Название файла
		$fileextension= pathinfo($temp[0], PATHINFO_EXTENSION); //Формат файла
        echo '<div class="doc-img">
                <input class="img-val" type="hidden" value="'.$data.'">
                <img src="http://wp/wp-includes/images/media/document.png" class="icon" draggable="false" alt="">
                <div> Название: '.$filename.' </div>
                <div style="display: block"> Формат: '.$fileextension.' </div>
                 <div style="display: block"> Размер файла: '.$temp[1].' </div>
                <input class="img-filename" type="hidden" value="'.$filename.'">
                <button class="button-del button ">удалить</button>
              </div>';
	}
	echo "</div>";
    echo '<input name="document-input" type="hidden" value="'.$data.'">
          <input name="filesizeHumanReadable" type="hidden" value="'.$data.'">
          </div>';
}

add_action('save_post', 'metabox_media_save');

function metabox_media_save($postID)
{
$terms2=get_the_terms($postID,'category');
	/// массив с названиями полей
	$names = array('media');

	foreach ($names as $key => $value) {

		// пришло ли поле наших данных?
		if (isset($_POST[$value])) {

			// не происходит ли автосохранение?
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				return;

			// не ревизию ли сохраняем?
			if (wp_is_post_revision($postID))
				return;


				// коррекция данных
				$data = sanitize_text_field($_POST[$value]);
			// запись
			 foreach ($terms2 as $cat) {
				if ($cat->slug=='news') {
         			update_post_meta($postID, "media", $data);
				}
			}
		}
	}
}


function metabox_media_showup($post, $box)
{
	wp_nonce_field('metabox_my_work_action', 'metabox_my_work_nonce');
	$data = get_post_meta($post->ID, 'media', true);
	$temp = explode(",", $data);
	echo '<div class="metabox-container">';
	echo '<div class="button-container"><button id="addmedia" class="button button-primary">Добавить</button></div>';
	echo '<div class="images-container">';
	if($temp[0]) {
		foreach ( $temp as $t_val ) {
			echo '<div class="img-cont">
				  <img src="'.$t_val.'" class="img" alt="" width="100px" height="100px">
				  <button class="button-del button ">удалить</button>
				  </div>';
		}
	}
	echo "</div>";
	echo '<input name="media" type="hidden" value="'.$data.'"></div>';

}

add_action('save_post', 'metabox_places_save');

function metabox_places_save($postID)
{
$terms=get_the_terms($postID,'category');

	$names = array("places");

	foreach ($names as $key => $value) {
		if (isset($_POST[$value])) {
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

			if (wp_is_post_revision($postID)) return;

			check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');

            $date=$_POST["date-input"];
			$data = sanitize_text_field($_POST[$value]);
            $fullData=$data.','.$date;

        	foreach ($terms as $cat) {
				if ($cat->slug=='events') {
         			update_post_meta($postID, "places", $fullData);
				}
			}
		}
	}
}

function metabox_places_showup($post, $box)
{

	$data = get_post_meta($post->ID, 'places', true);
	$temp = explode(",", $data);
	echo '<div>
		  <input name="places" type="text" value="'.$temp[0].'">
	      </div>';

}

add_action('save_post', 'metabox_date_save');

function metabox_date_save($postID)
{
    $terms=get_the_terms($postID,'category');
    $names = array("date-input");
    foreach ($names as $key => $value) {
        if (isset($_POST[$value])) {

            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
            if (wp_is_post_revision($postID)) return;

            check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');

            $data = sanitize_text_field($_POST[$value]);
            $fullData=$data;
            foreach ($terms as $cat) {
                if ($cat->slug=='events') {
                    update_post_meta($postID, "event_date", $fullData);
                } else {
                    update_post_meta($postID, "news_date", $fullData);
                }
            }
        }
    }
}

function metabox_date_showup($post, $box)
{
	$terms=get_the_terms($post->ID,'category');
	if($terms!=0) {
	    foreach ($terms as $cat) {
	        if ($cat->slug == 'events')

	            $data = get_post_meta($post->ID, 'event_date', true);
	        else
	            $data = get_post_meta($post->ID, 'news_date', true);
	    }
	}
		echo '<div><input name="date-input" value="'.$data.'" ></div>';

}

function metabox_page_showup($post, $box)
{
	$terms=get_the_terms($post->ID,'category');
	if($terms!=0) {
	    foreach ($terms as $cat) {
	        if (($cat->slug == 'events')&&(get_post_meta($post->ID, 'promo', true)==="yes")) {
                echo '<span>Да</span><input type="radio" name="promo-page" value="yes" checked="checked">
					 <span>Нет</span><input type="radio" name="promo-page" value="no">';
            } else {
                echo '<span>Да</span><input type="radio" name="promo-page" value="yes">
					  <span>Нет</span><input type="radio" name="promo-page" value="no" checked="checked">';
            }
	    }
    }
}


function metabox_link_showup($post, $box)
{
	$terms=get_the_terms($post->ID,'category');
	if($terms!=0) {
	    foreach ($terms as $cat) {
	        $data = get_post_meta($post->ID, 'other-link', true);
	    }
	}
	echo '<div><input name="other-link" value="'.$data.'" ></div>';
}


function metabox_main_showup($post, $box)
{
	$terms=get_the_terms($post->ID,'category');
	if($terms!=0) {
	    foreach ($terms as $cat) {
            if ((get_post_meta($post->ID, 'events-main', true) === "yes") || (get_post_meta($post->ID, 'news-main', true) === "yes") || (get_post_meta($post->ID, 'learn-main', true) === "yes") || (get_post_meta($post->ID, 'other-main', true) === "yes")) {
                echo '<span>Да</span><input type="radio" name="main-promo-page" value="yes" checked="checked">
                      <span>Нет</span><input type="radio" name="main-promo-page" value="no">';
            } else {
                echo '<span>Да</span><input type="radio" name="main-promo-page" value="yes">
                      <span>Нет</span><input type="radio" name="main-promo-page" value="no" checked="checked">';
                echo '<div>Фон для промо</div>';
            }
	  		$data = get_post_meta($post->ID, 'promo-image',true);
			echo '<div class="metabox-container">';
			echo '<div class="promo-container">';
			$temp = explode(",", $data);
			if(!$data) {
			    echo '<div class="button-container"><button id="addpromo" class="button button-primary add">Добавить</button></div>';
			} else {
				echo '  <div class="promo-img">
						<img src="'.$temp[0].'" class="img" alt="" width="100px" height="100px">
						<button class="button-del-promo button ">удалить</button>
						</div>';
			}
			echo "</div>";
			echo '<input name="promo-input" type="hidden" value="'.$data.'">
	  		</div>';
	    }
	}
}

add_action('save_post', 'metabox_promo_save');

function metabox_promo_save($postID)
{
	$terms=get_the_terms($postID,'category');
	$names = array("promo-page");
	foreach ($names as $key => $value) {
		if (isset($_POST[$value])) {
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return;

            if (wp_is_post_revision($postID))
                return;

            check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');
            $data = sanitize_text_field($_POST[$value]);
            foreach ($terms as $cat) {
                if ($cat->slug=='events') {
                    update_post_meta($postID, "promo", $data);
                }
            }
		}
	}
}

add_action('save_post', 'metabox_link_save');

function metabox_link_save($postID)
{
    $terms=get_the_terms($postID,'category'); //TODO: maybe not ?
    $names = array("other-link");
    foreach ($names as $key => $value) {
        if (isset($_POST[$value])) {
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return;
            if (wp_is_post_revision($postID))
                return;

            check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');

            $data = sanitize_text_field($_POST[$value]);

            if($terms!=0) {
                foreach ($terms as $cat) {
                    if ($cat->slug == 'other-link')
                        update_post_meta($postID, "other-link", $data);
                }
            }
        }
    }
}

add_action('save_post', 'metabox_main_save');

function metabox_main_save($postID)
{
	$terms=get_the_terms($postID,'category');
	$names = array("main-promo-page", "promo-input");
	foreach ($names as $key => $value) {
		if (isset($_POST[$value])) {
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
				return;
			if (wp_is_post_revision($postID))
				return;
			check_admin_referer('metabox_my_work_action', 'metabox_my_work_nonce');
            $data = sanitize_text_field($_POST[$value]);
            if ($value==='main-promo-page') {
                foreach ($terms as $cat) {
                    if ($cat->slug=='events') {
                        update_post_meta($postID, "events-main", $data);
                    } else if ($cat->slug=='news') {
                        update_post_meta($postID, "news-main", $data);
                    } else if ($cat->slug=='learn') {
                    update_post_meta($postID, "learn-main", $data);
                    } else if ($cat->slug=='other-link') {
                        update_post_meta($postID, "other-main", $data);
                    }
                }
            } else if ($value==='promo-input') {
                update_post_meta($postID, "promo-image", $data);
            }
		}
	}
}
