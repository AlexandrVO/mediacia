<?php get_header()?>
<div class="container-fluid no-padding  main">
    <div class="row  no-margin  ">
        <?php get_template_part('template-parts/breadcrumps')?>
            <div class="content" id="content">
                <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post(); ?>
                            <?php get_template_part('template-parts/content', get_post_format())?>
                            <?php endwhile; else: ?>
                            <div class="post-content">
                            <p>Извините, ничего не найдено.</p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer()?>
</div>

