<div class="container-fluid">
	<?php get_header()?>
        <div class="container-fluid no-padding  main">
            <div class="row  no-margin  ">
                <?php get_template_part('template-parts/breadcrumps')?>
                <div class="content" id="content">
                    <?php
                        if (have_posts()) :
                            while (have_posts()) :
                                the_post(); ?>
                            <?php if (in_category('learn')) {
                                    get_template_part('template-parts/learn', 'single') ;
                                } else {
                                get_template_part('template-parts/content', 'single');
                            }?>
                            <?php endwhile;
                        else: ?>
                            <div class="post-content">
                                <p>Извините, ничего не найдено.</p>
                            </div>
                        <?php endif; ?>
                </div>
            </div>
        </div>
	<?php get_footer()?>
</div>
