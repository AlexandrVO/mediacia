<?php
require('functions/breadcrumps-function.php');
require('functions/pagination_function.php');
require('functions/medi_shortcodes.php');
require('functions/medi-shortcodes-mediators.php');
require('functions/medi-shortcodes-events.php');
require('functions/medi-shortcodes-news.php');
require('functions/medi-shortcodes-learn.php');
require('functions/medi-shortcodes-contacts.php');
require('functions/medi_metaboxes.php');
require('functions/send_event.php');
require('functions/small_functions.php');

function medit_theme_setup()
{
    load_theme_textdomain('medit_theme');

    add_theme_support('title-tag');

    add_theme_support('custom-logo', array('height'=>31,'width'=>134, 'flex-height'=>true));

    add_theme_support('post-thumbnails');

    set_post_thumbnail_size(730, 446);

    add_theme_support('html5', array(
        'search_form',
        'comment-form',
        'comment-list',
        'gallery','
        caption'
    ));

    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'gallery',
        'link'
    ));
    register_nav_menu('primary', 'Primary menu');
    add_filter('acf/settings/remove_wp_meta_box', '__return_false');
}


function true_custom_fields()
{
    add_post_type_support('participant', 'custom-fields'); // в качестве первого параметра укажите название типа поста
}

add_action('init', 'true_custom_fields');
add_action('after_setup_theme', 'medit_theme_setup');

function medit_theme_admin_scripts($hook_suffix)
{
    wp_enqueue_script('met', get_template_directory_uri() . '/js/metadata.js');
    wp_enqueue_style('met_css', get_template_directory_uri() . '/css/metadata.css');
}

add_action('admin_enqueue_scripts', 'medit_theme_admin_scripts');

function medit_theme_map_scripts($hook_suffix)
{
    wp_enqueue_script('map', get_template_directory_uri() . '/js/map.js');
}
show_admin_bar(false);

function medit_theme_scripts()
{
    wp_enqueue_script('jquery', get_template_directory_uri() . '/libs/js/jquery-3.3.1.min.js');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/libs//bootstrap.min.css');
    wp_enqueue_style('style-css', get_stylesheet_uri());
    wp_enqueue_style('style-media', get_template_directory_uri().'/css//media.css');
    wp_enqueue_style('style-doc', get_template_directory_uri().'/css/documents.css');
    wp_enqueue_style('style-mediators', get_template_directory_uri().'/css/mediators.css');
    wp_enqueue_style('style-events', get_template_directory_uri().'/css/events.css');
    wp_enqueue_style('style-news', get_template_directory_uri().'/css/news.css');
    wp_enqueue_style('style-contacts', get_template_directory_uri().'/css/contacts.css');
    wp_enqueue_style('style-learn', get_template_directory_uri().'/css/learn.css');
    wp_enqueue_style('style-lich', get_template_directory_uri().'/css/lich.css');
    wp_enqueue_style('style-about', get_template_directory_uri().'/css/about.css');
    wp_enqueue_script('content', get_template_directory_uri() . '/js/index_content.js');
    wp_enqueue_script('map', get_template_directory_uri() . '/js/map.js');
    wp_enqueue_script('find', get_template_directory_uri() . '/js/find_dock.js');
    wp_enqueue_script('learn-js', get_template_directory_uri() . '/js/learn.js');

    if (get_the_ID()==37) {
        wp_enqueue_script('sort', get_template_directory_uri() . '/js/sort_events.js');
    }

    wp_enqueue_script('carousel', get_template_directory_uri() . '/js/carousel.js');
    wp_enqueue_style('owl-css', get_template_directory_uri().'/plugins/OwlCarousel/dist/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-css-ef', get_template_directory_uri().'/plugins/OwlCarousel/dist/assets/owl.theme.default.css');
    wp_enqueue_script('owl-js', get_template_directory_uri().'/plugins/OwlCarousel/dist/owl.carousel.min.js');
    wp_enqueue_script('des-js', get_template_directory_uri() . '/plugins/Slider-Pro/dist/js/jquery.sliderPro.min.js');
    wp_enqueue_style('bx-css', get_template_directory_uri().'/plugins/bxslider/dist/jquery.bxslider.css');
    wp_enqueue_script('bx-js', get_template_directory_uri() . '/plugins/bxslider/dist/jquery.bxslider.min.js');
}

add_action('wp_enqueue_scripts', 'medit_theme_scripts');
add_action('wp_enqueue_scripts', 'action_function_name_7714');

function action_function_name_7714()
{
    wp_enqueue_script('my-ajax-request', get_template_directory_uri() . '/js/send_event.js');
    wp_localize_script('my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url('admin-ajax.php') ));
}

function medit_theme_customize_register($wp_customize)
{
    $wp_customize->add_setting('header_mail', array(
        'default'   => __('admin@mail.ru', 'medit_theme'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('mail_section', array(
        'title'      => __('Изменить mail', 'medit_theme'),
        'priority'   => 30,
    ));

    $wp_customize->add_control(
        'header_mail',
        [
        'label'      => __('Header mail', 'medit_theme'),
        'section'    => 'mail_section',
        'settings'   => 'header_mail',
        'type'      => 'text',
        ]
    );

    $wp_customize->add_setting('header_phone', array(
        'default'   => __('666 666 666', 'medit_theme'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('phone_section', array(
        'title'      => __('Изменить телефон', 'medit_theme'),
        'priority'   => 30,
    ));

    $wp_customize->add_control(
        'header_phone',
        [
        'label'      => __('Header Phone', 'medit_theme'),
        'section'    => 'phone_section',
        'settings'   => 'header_phone',
        'type'      => 'text',
        ]
    );

    $wp_customize->add_setting('phone_contact', array(
        'default'   => __('666 666 661', 'medit_theme'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('contact_section', array(
        'title'      => __('Изменить второй телефон', 'medit_theme'),
        'priority'   => 32,
    ));

    $wp_customize->add_control(
    'phone_contact',
        [
        'label'      => __('Footer Phone', 'medit_theme'),
        'section'    => 'contact_section',
        'settings'   => 'phone_contact',
        'type'      => 'text',
        ]
    );

    $wp_customize->add_setting('adress', array(
        'default'   => __('бейкер стрит 221в', 'medit_theme'),
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('adress_section', array(
        'title'      => __('Изменить адрес', 'medit_theme'),
        'priority'   => 31,
    ));

    $wp_customize->add_control(
        'adress',
        [
        'label'      => __('Adress', 'medit_theme'),
        'section'    => 'adress_section',
        'settings'   => 'adress',
        'type'      => 'text',
        ]
    );
}

add_action('customize_register', 'medit_theme_customize_register');

function create_post_type_partners()
{
    register_post_type(
        'partner',
        [
        'labels' => [
        'name' => __('Партнеры'),
        'singular_name' => __('Партнер')
        ],
        'public' => true,
        'has_archive' => true,
        'supports'=> ['title','editor','thumbnail','post-format'],
        ]
    );
}

add_action('init', 'create_post_type_partners');

function create_post_type_mediators()
{
    register_post_type(
    'participant',
    array(
    'labels' => array(
    'name' => __('Участники'),
    'singular_name' => __('Участник')
    ),
    'public' => true,
    'has_archive' => true,
    'supports'=>array('title','editor','thumbnail','post-format'),
    )
);

register_taxonomy(
        'role',
        'participant',
        [
        'label' => __('Роль'),
        'rewrite' => array( 'slug' => 'role' ),
        ]
    );
}

add_action('init', 'create_post_type_mediators');

function create_post_type_learn_program()
{
    register_post_type(
    'program',
    array(
    'labels' => array(
    'name' => __('Программа Обучения'),
    'singular_name' => __('Программа')
    ),
    'public' => true,
    'has_archive' => true,
    'supports'=>array('title','editor','thumbnail','post-format'),
    )
);

register_taxonomy(
    'learn',
    'program',
    array(
    'label' => __('Обучение'),
    'rewrite' => array( 'slug' => 'learn' ),
    )
    );
}

add_action('init', 'create_post_type_learn_program');

function create_post_type_documents()
{
    register_post_type(
        'document',
        array(
        'labels' => array(
        'name' => __('Документы'),
        'singular_name' => __('Документ')
        ),
        'public' => true,
        'has_archive' => true,
        'supports'=>array('title','editor','thumbnail','post-format'),
        )
    );
}

add_action('init', 'create_post_type_documents');

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function medit_theme_widgets_init()
{
    register_sidebar(array(
        'name'          => __('My Sidebar', 'medit_theme'),
        'id'            => 'sidebar-1',
        'description'   => __('Widgets in this area will be shown on all posts and pages.', 'medit_theme'),
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
    ));
}
add_action('widgets_init', 'medit_theme_widgets_init');

add_filter('mce_buttons', 'mce_page_break');
function mce_page_break($mce_buttons)
    {
        $pos = array_search('wp_more', $mce_buttons, true);
        if ($pos !== false) {
        $buttons     = array_slice($mce_buttons, 0, $pos + 1);
        $buttons[]   = 'wp_page';
        $mce_buttons = array_merge($buttons, array_slice($mce_buttons, $pos + 1));
    }
    return $mce_buttons;
}
