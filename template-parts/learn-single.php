<article id="post-<?php the_ID();?>">
    <h3 class="the-title post-single"><?php the_title(); ?></h3>
		<div class="post-content">
						<?php
						the_content();
						// echo '<div class="container-fluid no-padding no-margin">
						// 		<div class="row">
						// 				<div class="col-sm-12 ">
						// 				<div>
						// 					<p class="post-single learn-text1"><b>Это программа повышения квалификации по направлению «Психолого-педагогическое образование».</b></p>
						// 				</div>
						// 				</div>
						// 				</div>
						// 		</div>



						// 		<div class="row">
						// 				<div class="col-lg-4 col-md-12 col-sm-12 no-padding pink-block">
						// 					<div class="">
						// 						<p class="post-single learn-text2"><b>Организация деятельности школьной службы примирения (медиации)</b></p>
						// 					</div>
						// 				</div>


						// 				<div class="col-lg-8 col-md-12 col-sm-12 no-padding pink-block2">
						// 					<div class="post-single">
						// 						<p class=" learn-text3">Новые социальные реалии порождают новые конфликты в школьном социуме, негативный эффект которых зачастую усиливается  в результате  Интернет-коммуникации, вовлечённых в него детей и взрослых.  Согласно данным ГУ МВД России по Ростовской области наблюдается рост преступлений с участием несовершеннолетних. Действенным инструментом формирования безопасной среды в образовательном учреждении, предупреждения конфликтов и развития культуры созидания является медиация.</p>

						// 							<p>Данная программа  реализуется в целях обеспечения эффективной сетевой проектной деятельности школ, в соответствии с Дорожной картой по развитию служб примирения  в образовательных организациях Ростовской области и Распоряжением Правительства РФ от 30.07.2014 № 1430.
						// 							</p>
						// 					</div>
						// 				</div>

						// 		</div>

						// 		<div class="row">
						// 			<div class="col-sm-12 ">
						// 				<div>
						// 					<p class="post-head post-single">Эта программа для Вас, если</p>
						// 				</div>
						// 			</div>
						// 		</div>

						// 		<div class="row post-single1">
						// 			<div class="col-lg-4 col-md-12 col-sm-12 ">
						// 				<div class="prog-block1">
						// 					<p class="learn-text4">Вы <span class="text-purple">директор школы</span> или <span class="text-purple">заместитель</span> по вопросам безопасности,  учебно-воспитательной и воспитательной работе, <span class="text-purple">социальный педагог, педагог- организатор, классный руководитель, психолог, уполномоченный по правам ребёнка</span>, а также педагог или сотрудник образовательного учреждения, взаимодействующий с детскими и подростковыми коллективами в рамках учебной и внеклассной работы</p>
						// 				</div>
						// 			</div>
						// 			<div class="col-lg-8 col-md-12 col-sm-12">
						// 				<div class=" prog-block">
						// 					<p class="learn-text4">Вы заинтересованы в развития культуры созидания и позитивного  общения, эффективной этнокультурной коммуникации в школьной среде, результативной  предупредительно-профилактической работе с  несовершеннолетними «группы риска»</p>
						// 				</div>
						// 				<div class="prog-block prog-pad">
						// 					<p class="learn-text4">Готовы использовать информационные технологии и новые медиа (социальные сети, блоги, форумы) в воспитательной работе и профилактике конфликтов.</p>
						// 				</div>
						// 			</div>
						// 		</div>';


								 // do_shortcode( '[program]' );


								// 	echo '<div class="row">
								// 		<div class="col-sm-12 ">
								// 			<div>
								// 				<p class="post-head post-single">В результате обучения Вы</p>
								// 			</div>
								// 		</div>
								// 	</div>


								// 	<div class="row">
								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">научитесь эффективно использовать  современные медиативные технологии  в работе школьной службы примирения  </p>
								// 			</div>
								// 		</div>

								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">получите знания  о школьной медиации, способах организации службы примирения в школе, методах оценки эффективности  её деятельности</p>
								// 			</div>
								// 		</div>

								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">овладеете медиативными технологиями развития культуры созидания в подростковой и молодёжной среде</p>
								// 			</div>
								// 		</div>


								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">освоите и сможете применять основные технологии медиации в  предупреждении и разрешении конфликтов, развитии культуры восстановительных взаимоотношений</p>
								// 			</div>
								// 		</div>


								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">научитесь на практикумах и тренингах применять медиативные технологии в воспитательной работе и  обеспечении безопасной среды</p>
								// 			</div>
								// 		</div>

								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">в результате участия в форсайт-мероприятиях получите представление о своих сильных компетенциях и зонах, требующих развития </p>
								// 			</div>
								// 		</div>

								// 		<div class="col-lg-4 col-md-6 col-sm-12 border-col">
								// 			<div class="prog-block2">
								// 				<p class="">нарастите свои компетенции в области коммуникации и влияния, предотвращения конфликтов и развитии продуктивного сотрудничества</p>
								// 			</div>
								// 		</div>


								// 	</div>

								// </div>';
            if (is_user_logged_in()==true)
            {
            $met=get_user_meta(get_current_user_id(), 'learns');
            foreach($met as $m => $value){
                if (get_the_ID()==$value)
                {$j=true;
                    break;}
                else{
                    $j=false;
                }
            }
            if ($j==true){
                echo '<div class="event-center"><span class="pink-event">Вы записаны на это мероприятие!</span></div>';}
            else {
                echo '<div id="event1" class="event-center">
                <input id="post_id" type="hidden" value="'.get_the_ID().'">
                   <input id="typ" type="hidden" value="learn">
				<button class="send-button-event" id="send_submit">Подать заявку</button>
			</div>';
            }
            }
            else{
                echo '<div class="learn-btn"><a  href="../login" class="send-button-event event-send-link">Подать заявку</a></div>';
            }
