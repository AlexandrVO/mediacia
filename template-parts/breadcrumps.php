<button class="navbar-toggler my-toggler-button" type="button" id="toggler">
    <span class="navbar-toggler-icon my-toggler-icon" id="openNav1">
        <svg id="SVGDoc" width="25" height="34" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 18 9">
          <defs>
            <path d="M351.56244,73.12197h16.87367c0.31049,0 0.56244,-0.25135 0.56244,-0.56099c0,-0.30968 -0.252,-0.56098 -0.56244,-0.56098h-16.87367c-0.31049,0 -0.56244,0.25135 -0.56244,0.56098c0,0.30964 0.252,0.56099 0.56244,0.56099z" id="Path-0"/>
             <path d="M368.43611,76h-16.87367c-0.31049,0 -0.56244,0.25135 -0.56244,0.56098c0,0.30964 0.252,0.56099 0.56244,0.56099h16.87367c0.31049,0 0.56244,-0.25135 0.56244,-0.56099c0,-0.30963 -0.25195,-0.56098 -0.56244,-0.56098z" id="Path-1"/>
            <path d="M362.81155,80h-11.24911c-0.31049,0 -0.56244,0.25135 -0.56244,0.56098c0,0.30969 0.252,0.56099 0.56244,0.56099h11.24911c0.31049,0 0.56244,-0.25135 0.56244,-0.56099c0.00005,-0.30968 -0.25195,-0.56098 -0.56244,-0.56098z" id="Path-2"/>
          </defs>
          <g transform="matrix(1,0,0,1,-351,-72)"><g><title>menu</title><g><title>Group 5</title><g><title>Shape</title><use xlink:href="#Path-0" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-1" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-2" fill="#ffffff" fill-opacity="1"/></g></g></g><g><title>Group 5</title><g><title>Shape</title><use xlink:href="#Path-0" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-1" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-2" fill="#ffffff" fill-opacity="1"/></g></g><g><title>Shape</title><use xlink:href="#Path-2" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-1" fill="#ffffff" fill-opacity="1"/></g><g><title>Shape</title><use xlink:href="#Path-0" fill="#ffffff" fill-opacity="1"/></g>
          </g>
        </svg>
       </span>
</button>
<div class="sidenav" id="sidenav">
		<div class="img-padding">
        <a class="logo-link" href="<?php echo get_home_url() ?>">
            <div class="logo">Центр медиации
            </div>
        </a>
</div>
  <div class="menu-body">
  	<?php  wp_nav_menu(array(
                   'theme_location'=>'primary',
                   'items_wrap'=>' <ul id="%1$s" class="%2$s">%3$s</ul>',
                   'menu_class'=>'menu',
                   'menu_id'=>'',
                   'depth'=>2
                  ));?>
     <div class="menu-footer">
       <div class="menu-footer-text">© 2018 ЦМ</div>
     </div>
  </div>
</div>
  <div class="sidenav1" id="sidenav1">
    <div class="img-padding"><a class="logo-link" href="<?php echo get_home_url() ?>"><div class="logo1">ЦМ</div></a></div>
      <div class="menu-body">
        <div class="menu-tog">
      </div>
      <div class="menu-footer"></div>
      </div>
</div>

