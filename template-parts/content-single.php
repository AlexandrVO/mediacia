<article id="post-<?php the_ID();?>" class="post-article">
	<?php
		$terms=get_the_terms(get_the_ID(),'category');
		foreach ($terms as $cat)
		{
			if ($cat->slug=='events'):?>
				<div class="row">
					<div class="col-sm-12 no-padding">
						<div class="post-image">
						<?php
							$th= get_post_thumbnail_id(get_the_ID());
							$u = wp_get_attachment_url($th);
							echo '<img src="'.$u.'"   class="event-promo-img">';?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12  post-single">
						<h3 class="post-head"><?php the_title(); ?></h3>
							<hr>
					</div>
				</div>
				<div class="row post-single">
					<div class="col-lg-7 col-md-12 col-sm-12">
						<div class="post-content ">
							<?php the_content();
								$met= get_post_meta(get_the_ID(), 'event_date', false);
							?>
						</div>
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12">
						<div class="info-block">
							<div class="info">
								<div class="info-block-date">
									<div class="info-block-date-dig">
										<?php
											$temp = explode("-", get_post_meta(get_the_ID(), 'event_date', true));
											echo intval($temp[2]);
										?>
									</div>
									<div class="info-block-date-dig-near">
										<span class="year-month">
											<span class="info-block-date-week"><?php get_week($met[0]);?></span>
											<span class="info-block-date-year"><?php echo $temp[0];?></span>
										</span>
										<span class="info-block-date-dig-month">
											<?php get_month($met[0])?>
										</span>
									</div>
									<div class="time">
										<?php echo get_post_meta(get_the_ID(), 'time', true)?>
									</div>
								</div>
								<div class="adress-block">
									<span><?php echo get_theme_mod('adress')?></span>
								</div>
								<div class="pink-text">
									<div class="pink-text1">
										<img>
										<p class="p1"><?php echo '<img class="imd-par" src="'.get_template_directory_uri().'/img/time.svg">'; ?>Длительность</p>
										<p class="p2"><?php echo get_post_meta(get_the_ID(), 'long', true)?></p>
								   </div>
								   <div class="pink-text2">
								   		<img>
								       <p class="p1"><?php echo '<img class="imd-par" src="'.get_template_directory_uri().'/img/par.svg">'; ?>Учаcтие</p>
								       <p class="p2"><?php echo get_post_meta(get_the_ID(), 'participant', true)?></p>
								   </div>
								</div>
								<div class="places-block">
									<span class="places">
										Осталось
										<?php
											$temp = explode(",", get_post_meta(get_the_ID(), 'places', true));
											echo $temp[0];
										?>
										свободных мест
									</span>
								</div>
					            <?php
					            	if (is_user_logged_in()==true)
					            	{
							            $met=get_user_meta(get_current_user_id(), 'event');
							            foreach($met as $m => $value)
							            {
							                if (get_the_ID()==$value)
							                {
							                	$k=true;
							                    break;
							                }
							                else
							                {
							                    $k=false;
						             		}
					            		}
					            		if ($k==true)
					            		{
					                		echo '<span class="pink-event">Вы записаны на это мероприятие!</span>';
					            		}
					            		else
					            		{
					                		echo '<div id="event1">
					                				<input id="post_id" type="hidden" value="'.get_the_ID().'">
					                   				<input id="typ" type="hidden" value="event">
													<button class="send-button-event1" id="send_submit">Подать заявку</button>
												</div>';
					           			}
					            	}
					            	else
					            	{
					                	echo '<a  href="../login" class="send-button-event1 event-send-link">Подать заявку</a>';
					            	}
					            ?>
							</div>
						</div>
					</div>
				</div>
			<?php elseif ($cat->slug=='news'):?>
				<div class="row">
					<div class="col-sm-12">
			       		<h3 class="post-head-news post-single"><?php the_title(); ?></h3>
					</div>
				</div>
				<div class="row post-single">
					<div class="col-sm-12">
			       		<p class="news-dig-single">
							<span>
								<?php
									$met= get_post_meta(get_the_ID(), 'news_date', false);
									$temp = explode("-", get_post_meta(get_the_ID(), 'news_date', true));
									echo intval(get_the_date('d'));?>
							</span>
							<span><?php the_date('F');?></span>
							<span><?php echo get_the_date('Y');?>г.</span>
						</p>
					</div>
				</div>
							<div class="row post-single1">
								<div class="col-md-12 col-lg-8">

									<script>
										jQuery(document).ready(function($){
										jQuery('.bxslider').bxSlider({
										   nextText: '',
										   prevText: '',
										   pagerCustom: '#bx-pager',
										   controls:true,
										});
										 $(window).resize(function() {
										         var h = $('#bxsl').css('height');
											$('#bx-pager').css('height', h);
											$('#bx-pager').css('overflow', 'auto');
										    })
										 var h0 = $('#bxsl').css('height');
											$('#bx-pager').css('height', h0);
										});
									</script>
									<div class="slider" id="bxsl">
									<ul class="bxslider">
									<?php
										$temp = explode(",", get_post_meta(get_the_ID(), 'media', true));
										foreach ($temp as $t)
										{
											echo '<li>
													<img class="sp-image1" src="'.$t.'"/>
												</li>';
										}
										?>
									 </ul>
									 </div>
								</div>
								<div class="col-sm-12 col-lg-3" id="bx-pager-1">
									<div class="parent" id="p">
								 		 <div class="child" id="bx-pager">
												  <?php
												  $k=0;
													foreach ($temp as $t)
													{
														echo '<a data-slide-index="'.$k.'" href=""><img class="sp-thumbnail" src="'.$t.'"/></a>';
														$k++;
													}
												?>
										</div>
									</div>
								</div>
							</div>
							<div class="row post-single">
								<div class="col-sm-12">
									<div class="news-content">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<div class="row post-single">
								<div class="col-sm-12 no-padding no-margin">
									<div class="other-news-text">
										Другие новости
									</div>
								</div>
							</div>
							<div class="container-fluid">
								<div class="row post-single">
									<?php
										$args2 = array(
											    'post_type' => 'post',
											    'exclude' => get_the_ID(),
									            'category_name'=>'news',
									            'posts_per_page' => 3,
											    	  );
												$query2 = new WP_Query();
												$my_post2 = $query2->query($args2);
												foreach ($my_post2 as $p)
												{
													$thumbId2 = get_post_thumbnail_id($p->ID);
													$thymbUrl2 = wp_get_attachment_url($thumbId2);
													$met= get_post_meta($p->ID, 'news_date', false);
													$temp = explode("-", get_post_meta($p->ID, 'news_date', true));
													echo '<div class="col-lg-4 col-md-6 col-sm-12 no-margin no-padding border-col">
									 		  				<div class="news-block">
																<a href="'.get_permalink($p->ID).'">
																	<img src="'.$thymbUrl2.'" alt="'. $p->post_title.'"  class="small-img img-responsive" align="center">
																</a>
																<div class="excerpt">
																	'.apply_filters( 'the_content', $p->post_excerpt).'
																</div>
																<p class="news-dig">
																	<span>'.intval(get_the_date('d')).'</span>
																	<span>'.get_the_date('F').'</span>
																	<span>'.get_the_date('Y').'г.</span>
																</p>
															</div>
															</div>';
												}
									?>
								</div>
							</div>
			<?php endif; ?>
		<?php } ?>
</article>

