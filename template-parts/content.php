<div class="container-fluid no-padding no-margin">
<article class="" id="post-<?php the_ID();?>">
	<?php
   if(is_page(37))
   {
   echo '<div class="event-block">
			<div class="owl-carousel owl-theme" id="event">';
				$args = array(
				'post_type' => 'post',
		         'category_name'=>'events',
			    );
				$query = new WP_Query();
				$my_post = $query->query($args);
					foreach ($my_post as $item)
					{
						if (get_post_meta($item->ID, 'promo', true)==="yes")
						{
							$thumbId = get_post_thumbnail_id($item->ID);
							$thymbUrl = wp_get_attachment_url($thumbId);
							echo '	<div class="item event-promo" >
										<img src="'.$thymbUrl.'"   class="event-promo-img">
									</div>';
						}
					}
	echo '</div>
		</div>';
	}?>
	<?php if (is_page(31)):?>
		<div class="row no-padding no-margin">
			<div class="col-lg-5 col-md-12">
				<div class=" post-single-left">
   			<div class="the-title"><?php the_title(); ?></div>
   			<?php the_content();?>
   			</div>
   			  </div>
   			<div class="col-lg-7 col-md-12 no-margin no-padding">
   			<div id="map" class="map"></div>
   			</div>
		</div>
	<?php elseif(is_page(109)):?>
		<?php if (is_user_logged_in()==false):?>
		<meta http-equiv = 'refresh' content = '0; url = login' />
		<?php else :?>
					<?php the_content();
			 endif; ?>
			 <?php elseif(is_page(83)||is_page(85)||is_page(86)):?>
			 	<div class="container-fluid no-padding ">
			 		<div class="log-area grey-background">
			 	 	<div class="row col-lg-8 col-md-12 col-sm-12 offset-lg-2 offset-sm-3 no-m">
			             <div class="log-form">
						 	<div class="the-title">
					   			<?php the_title(); ?>
								</div>
								<?php the_content(); ?>
								</div>
							</div>
						</div>
				</div>
				 <?php else:?>
				<div class="the-title post-single">
					<div class="row">
						<div class="col-12">
			   			<?php the_title(); ?>
			   		</div>
			        </div>
				</div>
					<?php the_content();
		  endif; ?>
</article>
</div>