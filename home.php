<?php get_header()?>
<div class="container-fluid no-padding  main">
    <div class="row no-margin">
        <?php get_template_part('template-parts/breadcrumps')?>
        <div class="content" id="content">
            <div class="container-fluid no-padding no-margin abs">
                <div class="full">
                    <div class="owl-carousel owl-theme" id="home-carousel">
                        <?php
                        $args = ['post_type' => 'post'];
                        $query = new WP_Query();
                        $my_post = $query->query($args);
                        foreach ($my_post as $item) {
                            if (((get_post_meta($item->ID, 'events-main', true)==="yes")||(get_post_meta($item->ID, 'news-main', true)==="yes")||(get_post_meta($item->ID, 'learn-main', true)==="yes")||(get_post_meta($item->ID, 'other-main', true)==="yes"))&&(get_post_meta($item->ID, 'promo-image', true)!=null)) {
                                $thumbId = get_post_thumbnail_id($item->ID);
                                $thymbUrl = wp_get_attachment_url($thumbId);
                                $terms=get_the_terms($item->ID, 'category');
                                $back=get_post_meta($item->ID, 'promo-image', true);
                                echo '<div class="image-slide" style="background-image: url('.$back.')">
                                    <div class="shadow-top">
                                    <div class="row no-padding no-margin">
                                    <div class="col-lg-6 offset-2">
                                    <div class="shadow-top-main-text">
                                    <p class="shadow-top-main-text-text1">'.$item->post_title.'</p>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="row no-padding no-margin">
                                    <div class="col-sm-7 offset-2">
                                    <div class="shadow-top-main-text-small">
                                    '.apply_filters('the_content', $item->post_excerpt).'
                                    </div>';
                                foreach ($terms as $cat) {
                                    if ($cat->slug=='other-link') {
                                        echo '<a  href="'.get_post_meta($item->ID, 'other-link', true).'" class="btn-a">
                                            <div class="send-button-main" >
                                            <span class="send-button-main-a">Узнать больше</span>
                                            </div>
                                            </a>';
                                    } else {
                                        echo '<a  href="'.get_permalink($item->ID).'" class="btn-a">
                                            <div class="send-button-main" >
                                            <span class="send-button-main-a">Узнать больше</span>
                                            </div>
                                            </a>';
                                    }
                                };
                                echo '
                                    </div>
                                    </div>
                                    </div>
                                    </div>';
                            }}?>
                    </div>
                </div>
                <div class="row no-padding no-margin">
                    <div class="main-pink-line">
                        <div class="sl1">
                            <div class="owl-carousel owl-theme" id="home-carousel-event">
                                <?php
                                $argse = array(
                                    'post_type' => 'post',
                                    'category_name'=>'events',
                                    'posts_per_page' => 5,
                                );
                                $querye = new WP_Query();
                                $my_poste = $querye->query($argse);
                                foreach ($my_poste as $poste) {
                                    $met= get_post_meta($poste->ID, 'event_date', false);
                                    $temp = explode("-", get_post_meta($poste->ID, 'event_date', true));
                                    echo '
                                <div class="slide-dig-event2">
                                <div class="slide-dig-event">
                                '.intval($temp[2]).'
                                </div>
                                <div class="slide-dig-event1">
                                ';
                                    echo get_month($met[0]);
                                    echo '
                                </div>
                                <div class="slide-text-event">
                                <a class="home-event-title" href="'.get_permalink($poste->ID).'">'.$poste->post_title.'</a>
                                </div>
                                </div>';
                                }?>
                            </div>
                        </div>
                        <span class="text-pink">Ближайшие мероприятия</span>
                        <div class="dark-pink-line-with-text"></div>
                        <div class="dark-purple-line"></div>
                        <div class="dark-pink-line">
                            <div class="dark-pink-line-text"></div>
                        </div>
                    </div>
                </div>
                <div class="row no-padding no-margin">
                    <div class="col-sm-12">
                        <p class="news-title-text">Новости</p>
                    </div>
                </div>
                <div class="row no-padding no-margin home-news-row">
                    <?php
                    $args2 = array(
                        'post_type' => 'post',
                        'category_name'=>'news',
                        'posts_per_page' => 5,
                    );
                    $query2 = new WP_Query();
                    $my_post2 = $query2->query($args2);
                    ?>
                    <div class="col-lg-6  col-md-12 rel">
                        <?php
                        $thumbId2 = get_post_thumbnail_id($my_post2[0]->ID);
                        $thymbUrl2 = wp_get_attachment_url($thumbId2);
                        $met= get_post_meta($my_post2[0]->ID, 'news_date', false);
                        $temp = explode("-", get_post_meta($my_post2[0]->ID, 'news_date', true));
                        echo '
                            <div class="news-block">
                            <a href="'.get_permalink($my_post2[0]->ID).'">
                            <img src="'.$thymbUrl2.'" alt="'. $my_post2[0]->post_title.'"  class="small-img img-responsive" align="center"></a>
                            <div class="home-news-title">'.apply_filters('the_content', $my_post2[0]->post_title).'</div>
                            <div class="home-news-excerpt">'.apply_filters('the_content', $my_post2[0]->post_excerpt).'</div>
                            <p class="news-dig">
                            <span>'.intval(get_the_date('d')).'</span>
                            <span>'.get_the_date('F').'</span>
                            <span>'.get_the_date('Y').'г.</span>
                            </p>
                            <a href="/news/" class="home-news-a">
                            <div class="home-news-link">
                            Посмотреть предыдущие новости
                            </div>
                            </a>
                            </div>';?>
                    </div>
                    <div class="col-lg-6 col-md-12 news-small-home">
                        <div class="row">
                            <?php
                            foreach ($my_post2 as $p) {
                                if ($p!=$my_post2[0]) {
                                    $thumbId2 = get_post_thumbnail_id($p->ID);
                                    $thymbUrl2 = wp_get_attachment_url($thumbId2);
                                    $met= get_post_meta($p->ID, 'news_date', false);
                                    $temp = explode("-", get_post_meta($p->ID, 'news_date', true));
                                    echo '<div class="col-lg-6 col-md-12 no-margin no-padding border-col-1">
                                    <a href="'.get_permalink($p->ID).'" class="news-link-block">
                                    <div class="news-block1">
                                    <img src="'.$thymbUrl2.'" alt="'. $p->post_title.'"  class="small-img img-responsive" align="center">
                                    <div class="home-news-excerpt2">'.apply_filters('the_content', $p->post_title).'</div>
                                    <p class="news-dig">
                                    <span>'.intval(get_the_date('d')).'</span>
                                    <span>'.get_the_date('F').'</span>
                                    <span>'.get_the_date('Y').'г.</span></p>
                                    </div>
                                    </a>
                                    </div>';
                                }
                            }?>
                        </div>
                    </div>
                </div>
                <div class="row no-padding no-margin grey-background">
                    <div class="col-12 no-padding post-single-first">
                        <div class="home-learn-box">
                            <?php
                            $args3 = array(
                                'post_type' => 'post',
                                'category_name'=>'learn',
                                'orderby' => 'post_title'
                            );
                            $query3 = new WP_Query();
                            $my_post3 = $query3->query($args3);
                            foreach ($my_post3 as $item3) {
                                $thumbId3 = get_post_thumbnail_id($item3->ID);
                                $thymbUrl3 = wp_get_attachment_url($thumbId3);
                                echo '
                                <div class="learn-item-home" id="'.$item3->ID.'">
                                <img src="'.$thymbUrl3.'" alt="'.$item3->post_title.'"  class="home-learn-img">
                                <div class="home-learn-title">
                                <a href="'.get_permalink($item3->ID).'">'.$item3->post_title.'</a>
                                </div>
                                </div>';
                            } ?>
                        </div>
                    </div>
                </div>
                <div class="row no-padding no-margin grey-background">
                    <div class="col-sm-12">
                        <p class="news-title-text">Медиаторы</p>
                    </div>
                </div>
                <div class="row no-padding no-margin">
                    <div class="col-md-12 col-lg-5 create-box-home">
                        <p class="news-title-text">Территория созидания</p>
                        <div class="territory-box">
                            <div class="territiry-text">
                                Развитие культуры восстановительных взаимоотношений в школе, содействие в обеспечении безопасности образовательной среды.
                            </div>
                            <div class="home-creation-link">
                                <a href="/creation-territory/">Узнать болшьше о проекте</a>
                            </div>
                            <div class="territiry-text1">
                                Проект создан в соответствии со стратегическим ориентирами воспитания, сформулированным Президентом РФ В. В. Путиным.
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-7 create-box-home1">
                        <?php echo'<div>
                            <img src="'.get_template_directory_uri().'/img/creat.png" class="create-image">
                            </div>'?>
                    </div>
                </div>
                <div class="row no-padding no-margin">
                    <div class="col-sm-12 no-padding">
                        <div id="map" class="map map-h"></div>
                    </div>
                </div>
                <div class="row no-padding  no-padding info-box">
                    <div class="home-adress col-lg-5">
                        <?php echo get_theme_mod('adress')?>
                    </div>
                    <div class="col-lg-3">
                        <div class="home-tel"><?php echo get_theme_mod('header_phone')?></div>
                        <div class="home-tel1"><?php echo get_theme_mod('phone_contact')?></div>
                    </div>
                    <div class="home-mail col-lg-4">
                        <?php echo get_theme_mod('header_mail')?>
                    </div>
                </div>
                <div class="row no-padding no-margin">
                    <?php
                    $args4 = array(
                        'post_type' => 'partner',
                        'orderby' => 'post_title'
                    );
                    $query4 = new WP_Query();
                    $my_post4 = $query4->query($args4);
                    foreach ($my_post4 as $item4) {
                        $thumbId = get_post_thumbnail_id($item4->ID);
                        $thymbUrl = wp_get_attachment_url($thumbId);
                        echo'
                                <div class="partner-item">
                                <a href="#"><img src="'.$thymbUrl.'"></a>
                                </div>';
                    }?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <?php get_footer()?>
    </div>
</div>

