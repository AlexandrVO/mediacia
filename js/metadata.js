jQuery(document).ready(function($){

	function formatSize(length) {
		var i = 0, type = ['б','Кб','Мб','Гб','Тб','Пб'];
		while((length / 1000 | 0) && i < type.length - 1) {
			length /= 1024;
			i++;
		}
		return length.toFixed(2) + ' ' + type[i];
	}

	var myplugin_media_upload;

	$('#adddocument').on('click', function(event) {
		event.preventDefault();
        if( myplugin_media_upload ) {
            myplugin_media_upload.open();
            return;
        }
        myplugin_media_upload = wp.media.frames.file_frame = wp.media({
            title: "Добавить Документ",
            button: { text: "Выбрать" },
            multiple: false //разрешить множественный выбор
        });

        /**
         *THE KEY BUSINESS
         *When multiple images are selected, get the multiple attachment objects
         *and convert them into a usable array of attachments
         */
        myplugin_media_upload.on( 'select', function(){
            var attachments = myplugin_media_upload.state().get('selection').map(
                function( attachment ) {
                	attachment.toJSON();
                    return attachment;
            });

            //loop through the array and do things with each attachment

	        for (var i = 0; i < attachments.length; ++i) {
	        	window.flag = true;
	        	window.ach = attachments[i];
	        	$('.metabox-container .document-container img').each(function(index, el) {
	        		if($(el).attr('src') == window.ach.attributes.url) {
						window.flag = false;
					}
		    	});
	        	if(window.flag){
					$('.metabox-container .document-container').append('<div class="doc-img"><img src="http://wp/wp-includes/images/media/document.png"><div>'+ach.attributes.title+ ' '+ formatSize(ach.attributes.filesizeInBytes)  +'</div><button class=" button button-del">удалить</button></div>')
					$(this).parent('.add').remove();
					var text = $('input[name ="document-input"]').val();
					text=ach.attributes.url+','+formatSize(ach.attributes.filesizeInBytes);
					$('input[name ="document-input"]').val(text);
					$('input[name ="title"]').val(ach.attributes.title);
	        	}
	        }
	        window.flag = true;
        	window.ach = '';
        });
    	myplugin_media_upload.open();
		});
	$('.document-container').on('click', '.button-del', function(event) {
		event.preventDefault();
		window.src = $(this).parent('.doc-img').children('.img').attr('src');
		window.doc = '';
		$('input[name ="document-input"]').val(window.doc);
		window.src = '';
		$(this).parent('.doc-img').remove();
	});

	var myplugin_media_upload;
	$('#addmedia').on('click', function(event) {
		event.preventDefault();
        if( myplugin_media_upload ) {
            myplugin_media_upload.open();
            return;
        }
        myplugin_media_upload = wp.media.frames.file_frame = wp.media({
            title: "Добавить",
            button: { text: "Выбрать" },
            multiple: true //allowing for multiple image selection
        });

        /**
         *THE KEY BUSINESS
         *When multiple images are selected, get the multiple attachment objects
         *and convert them into a usable array of attachments
         */
        myplugin_media_upload.on( 'select', function() {
            var attachments = myplugin_media_upload.state().get('selection').map(
                function( attachment ) {
                	attachment.toJSON();
                    return attachment;
            });

            //loop through the array and do things with each attachment

	        for (var i = 0; i < attachments.length; ++i) {
	        	window.flag = true;
	        	window.ach = attachments[i];
	        	if(window.flag){
	        		$('.metabox-container .images-container').append('<div class="img-cont"><img src="'+ ach.attributes.url +'" alt="" width="100px" height="100px"><button class="button-del button ">удалить</button></div>')
	        		var text = $('input[name ="media"]').val();
	        		if(text == '') {
						text = ach.attributes.url;
					} else {
						text += ',' + ach.attributes.url;
					}
	        		$('input[name ="media"]').val(text);
	        	}
	        }
	        window.flag = true;
        	window.ach = '';
        });

    	myplugin_media_upload.open();
	});

	$('.images-container').on('click', '.button-del', function(event) {
		event.preventDefault();
		window.src = $(this).parent('.img-cont').children('img').attr('src');
		window.med = '';
		var text = $('input[name ="media"]').val(),
		arr = text.split(',');
		$(arr).each(function(index, value) {
			if(value != window.src){
				if(window.med == '') {
					window.med = value;
				} else {
					window.med += ',' + value;
				}
			}
		});
		$('input[name ="media"]').val(window.med);
		window.src = '';
		window.med = '';
		$(this).parent('.img-cont').remove();
	});

var myplugin_media_upload;
	$('#addpromo').on('click', function(event) {
		event.preventDefault();
		// If the uploader object has already been created, reopen the dialog
        if( myplugin_media_upload ) {
            myplugin_media_upload.open();
            return;
        }
        // Extend the wp.media object
        myplugin_media_upload = wp.media.frames.file_frame = wp.media({
            //button_text set by wp_localize_script()
            title: "Добавить Фото",
            button: { text: "Выбрать" },
            multiple: false //разрешить множественный выбор

        });

        /**
         *THE KEY BUSINESS
         *When multiple images are selected, get the multiple attachment objects
         *and convert them into a usable array of attachments
         */
        myplugin_media_upload.on( 'select', function(){
            var attachments = myplugin_media_upload.state().get('selection').map(
                function( attachment ) {
                	attachment.toJSON();
                    return attachment;
            });

            //loop through the array and do things with each attachment

	        for (var i = 0; i < attachments.length; ++i) {
	        	window.flag = true;
	        	window.ach = attachments[i];
	        	$('.metabox-container .promo-container img').each(function(index, el) {
	        		if($(el).attr('src') == window.ach.attributes.url) {
						window.flag = false;
					}
		    	});

	        	if(window.flag) {
					$('.metabox-container .promo-container').append('<div class="promo-img"><img src="'+ ach.attributes.url +'" class="img" alt="" width="100px" height="100px"><button class="button-del-promo button ">удалить</button></div>')
					$(this).parent('.add').remove();
					var text = $('input[name ="promo-input"]').val();
					text=ach.attributes.url;
					$('input[name ="promo-input"]').val(text);
	        	}
	        }
	        window.flag = true;
        	window.ach = '';
        });

    	myplugin_media_upload.open();
	});

		$('.promo-container').on('click', '.button-del-promo', function(event) {
		event.preventDefault();
		window.src = $(this).parent('.promo-img').children('.img').attr('src');
		window.doc = '';
		$('input[name ="promo-input"]').val(window.doc);
		window.src = '';
		$(this).parent('.promo-img').remove();
	});
});


  

