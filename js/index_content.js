jQuery(document).ready(function($) {
$('.my-toggler-button').on('click', function(){
     if ($('#sidenav').width()=='300') {
            $('#sidenav').css('width','0');
            $('#toggler').css('left','6px');
            $('#sidenav1').css('width','70px');
            $('#toggler').css('top', '0');
        } else {
            $('#sidenav').css('width','300px');
            $('#sidenav1').css('width','0');
            $('#toggler').css('left','244px');
            $('#toggler').css('top', '-80px');
        }
});

$('.menu-item-has-children').on('click', function(){
    if ($(this).hasClass('sub-act')) {
            $(this).children('.sub-menu').css('display','none');
            $(this).removeClass('sub-act');
        } else {
            $(this).children('.sub-menu').css('display','block');
            $(this).addClass('sub-act');
        }
});


$('.func-center').on('click', function() {
    var id=$(this).attr('id');
    if ($('#under'+id).css('display') == 'none') {
            $('#under'+id).css('display', 'block');
        } else {
            $('#under'+id).css('display', 'none');
        }
    });
});
