jQuery(document).ready(function($)
{
  $('#find_doc').on('input', function () {
      var search = document.getElementById('find_doc').value;
  $.ajax({
    url: MyAjax.ajaxurl,
    type: 'POST',
    datatype: 'json',
    data: {
          action:'finddoc',
          search:search,
    },
    success: function (res) {
        res = JSON.parse(res);
        var countOfdocs = res.length;
         if (countOfdocs==0) {
            document.getElementById('update1').innerHTML='';
            var item  = document.createElement('div');
            item.innerHTML = '<span class="post-title">Документов по запросу не найдено</span>';
            document.getElementById('update1').appendChild(item);
        } else {
           document.getElementById("update1").innerHTML='';
          for (countOfdocs in res) {
            var id = res[countOfdocs].ID;
            var title = res[countOfdocs].post_title;
            var meta = res[countOfdocs].meta_value;
            var metaArray = meta.split(',');
            var url = '';
            var size = '';

            $(metaArray).each(function( index ) {
                switch(index) {
                  case 0:
                  url = metaArray[index];
                  break;
                  case 1:
                  size = metaArray[index];
                  break;
                  default:
                }
            });

            var formatArray = url.split('.');
            var k = 0;

            formatArray.forEach(function(formatArray) {
                k = k + 1;
            });

            format = formatArray[k-1];

            var item  = document.createElement('div');
            item.innerHTML=
                '<div>\n' +
                '<img src="http://mediation-center.ru/wp-content/themes/medit_theme/img/doc.svg">\n'+
                '<a href="'+url+'" class="post-title">'+ title+'</a>\n' +
                '<div class="doc-parametrs-text"><span>Размер: '+size+'</span>\n'+
                'Формат: <span class="format">'+format+'</span>\n'+
                '</div>\n' +
                '</div>\n';
                document.getElementById('update1').appendChild(item);
          }
        }
      }
    });
  });
});
