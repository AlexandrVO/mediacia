ymaps.ready(init);

function init(){
    var myMap = new ymaps.Map("map", {
        center: [47.2230, 39.7180],
        zoom: 16,
        controls: []
    });
    var myPlacemark = new ymaps.Placemark([47.2230, 39.7180], {
         hintContent: 'Содержимое всплывающей подсказки',
         balloonContent: 'Содержимое балуна'
    });
    myMap.geoObjects.add(myPlacemark);
}

jQuery(document).ready(function($) {
    $('.default-role').on('mouseover', function() {
        var man_id=$(this).attr('id');
        $('#cont'+man_id).css('display', 'none');
        $('#cont-act'+man_id).css('display', 'inline-block');
    });

    $('.default-role').on('mouseout', function() {
        var man_id=$(this).attr('id');
        $('#cont-act'+man_id).css('display', 'none');
        $('#cont'+man_id).css('display', 'inline-block');
    });

    $('.default-role').on('click', function() {
        var man_id=$(this).attr('id');
        if ($('#info'+man_id).css('display') == 'none') {
            $('#info'+man_id).css('display', 'block');
            $(this).addClass( "contacts-role-name-active" );
            $('#cont'+man_id).addClass( "cont2" );
            $('#cont-act'+man_id).addClass( "cont1" );
        } else {
            $('#cont'+man_id).removeClass( "cont2" );
            $('#cont-act'+man_id).removeClass( "cont1" );
            $('#info'+man_id).css('display', 'none');
            $(this).removeClass( "contacts-role-name-active" );
        }
    });
});
