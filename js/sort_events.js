jQuery(document).ready(function($) {
  function sortEvent(value){
  var btn=value;
  $.ajax({
    url: MyAjax.ajaxurl,
    type: 'POST',
    datatype: 'json',
    data: {
          action:'sort',
          btn:btn,
    },
    success: function (res) {
        res = JSON.parse(res);
        var countOfres = res.length;
        document.getElementById("update").innerHTML='';
        for (countOfres in res) {
            var name = res[countOfres].post_name;
            var title = res[countOfres].post_title;
            var meta = res[countOfres].meta_value;
            var item  = document.createElement('div');
            var month ='';
            var day='';
            if(meta==='') {
            day='';
            } else {
                var metaArray = meta.split('-');
                day=parseInt(metaArray[2], 10);
            }

            var month_s=parseInt(meta.split('-')[1],10)
            switch(month_s) {
                case 1:
                month='Января';
                break;
                case 2:
                month='Февраля';
                break;
                case 3:
                month='Марта';
                break;
                case 4:
                month='Апреля';
                break;
                case 5:
                month='Мая';
                break;
                case 6:
                month='Июня';
                break;
                case 7:
                month='Июля';
                break;
                case 8:
                month='Августа';
                break;
                case 9:
                month='Сентября';
                break;
                case 10:
                month='Октября';
                break;
                case 11:
                month='Ноября';
                break;
                case 12:
                month='Декабря';
                break;
                default:
                month='Дата уточняется';
            }

            item.className = "col-lg-4 col-md-6";
            item.innerHTML=
            '<div class="item-sort">\n' +
            '<a class="item-sort-title" href="../'+name+'">'+title+'</a>\n'+
            '<div class="date"><span class="date-dig">'+day+'</span><span class="month">'+month+'</span></div>\n'+
            '</div>\n' +
            '</div>\n';
            document.getElementById('update').appendChild(item);
            }
      }
    });
 }

 document.getElementById('date').classList.add("active-date");
 var btn = "date";
 sortEvent(btn);

  $('.sort-text').on('click', function () {
    document.getElementById('date').classList.remove("active-date");
    var btn = $(this).attr('id');
    if (btn==="date") {
        this.classList.add("active-date");
        document.getElementById('alf').classList.remove("active-date");
        document.getElementById('pop').classList.remove("active-date");
      }
    else if (btn==="alf") {
        this.classList.add("active-date");
        document.getElementById('date').classList.remove("active-date");
        document.getElementById('pop').classList.remove("active-date");
     }
    else if (btn==="pop") {
        this.classList.add("active-date");
        document.getElementById('date').classList.remove("active-date");
        document.getElementById('alf').classList.remove("active-date");
     }
    sortEvent(btn);
  });
});
